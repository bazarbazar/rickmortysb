package bazarbazaraps.rickapp

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log

class Permissions() {

    val TAG = "PermissionDemo"
    val RECORD_REQUEST_CODE = 101
    fun makeRequest(context: Context?) {
        ActivityCompat.requestPermissions(context as Activity,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                RECORD_REQUEST_CODE)


    }

    fun setupPermissions(context: Context?) {
        val permission = ContextCompat.checkSelfPermission(context as Activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.i(TAG, "Permission to record denied")
            makeRequest(context)
        }


    }

}