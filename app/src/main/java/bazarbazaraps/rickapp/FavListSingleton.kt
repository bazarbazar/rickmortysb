package bazarbazaraps.rickapp

class FavListSingleton {

    var fav_table: ArrayList<String> = ArrayList<String>()
    var fav_names_table: ArrayList<String> = ArrayList<String>()

    companion object {

        val instance: FavListSingleton  by lazy{ FavListSingleton() }
    }

}