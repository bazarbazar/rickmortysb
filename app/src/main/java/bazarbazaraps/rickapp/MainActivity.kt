package bazarbazaraps.rickapp

import android.app.AlarmManager
import android.app.Notification
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.view.*
import android.widget.*
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import android.content.ContentValues
import android.icu.math.BigDecimal
import android.media.RingtoneManager
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import android.support.v4.app.AlarmManagerCompat
import android.view.KeyEvent
import bazarbazaraps.rickapp.R.id.fav_button
import kotlinx.android.synthetic.main.activity_main.*
import com.startapp.android.publish.adsCommon.StartAppAd
import com.startapp.android.publish.adsCommon.StartAppSDK

class MainActivity : AppCompatActivity() {



    private lateinit var mp: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//AD
        //StartAppSDK.init(this, "205550775", true);
        StartAppSDK.init(this, "123", true);
     //  mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"

        mfav_button.setOnClickListener(){
            val intent = Intent(this,FavActivity::class.java)
            startActivity(intent)
        }
//AD
        val permissions  = Permissions()


          val  sample_names_const = arrayListOf<String>(
                  "aids",
                  "alongtimeago",
                  "andthatsthewaynewsgoes",
                  "andthatswhyialwayssay",
                  "awwwbitch",
                  "babylegs",
                  "burgertime",
                  "butthole",
                  "disqualified2",
                  "getschwiftyinhere",
                  "getschwiftymusicvideorickandmortyadultswim",
                  "goodjob",
                  "help",
                  "hiimmrmeeseekslookatme",
                  "hitthesackjack",
                  "idontgiveafkyouthink",
                  "ilikewhatyougot",
                  "ilovemorty",
                  "imantsinmyeyejohnson",
                  "inalieninvasion",
                  "inbirdculturethisisconsidered",
                  "itsgazorpazorpfield",
                  "letmeout",
                  "lickmyballs",
                  "myman",
                  "nobodyexists",
                  "ohman",
                  "ohmygod",
                  "ohyeahh",
                  "ooooyeahcaaandoo",
                  "owee",
                  "rick",
                  "rickytickytabbybiatch",
                  "riggity",
                  "rubberbabybabybunkers",
                  "screamingsun",
                  "showmewhatyougot",
                  "shutthefuckupaboutmoonman",
                  "thankyou",
                  "thatsretarded",
                  "thisishowyoudreambitch",
                  "what",
                  "whatever",
                  "whothefuck",
                  "woovuluvubdubdub",
                  "yesfuckyougodnottodaybitch"

            )

            val  MP_instances = ArrayList<Int>()
//var x = ("R.rav."+sample_names_const[0]).toInt()
        MP_instances.add(R.raw.aids)
        MP_instances.add(R.raw.alongtimeago)
        MP_instances.add(R.raw.andthatsthewaynewsgoes)
        MP_instances.add(R.raw.andthatswhyialwayssay)
        MP_instances.add(R.raw.awwwbitch)
        MP_instances.add(R.raw.babylegs)
        MP_instances.add(R.raw.burgertime)
        MP_instances.add(R.raw.butthole)
        MP_instances.add(R.raw.disqualified2)
        MP_instances.add(R.raw.getschwiftyinhere)
        MP_instances.add(R.raw.getschwiftymusicvideorickandmortyadultswim)
        MP_instances.add(R.raw.goodjob)
        MP_instances.add(R.raw.help)
        MP_instances.add(R.raw.hiimmrmeeseekslookatme)
        MP_instances.add(R.raw.hitthesackjack)
        MP_instances.add(R.raw.idontgiveafkyouthink)
        MP_instances.add(R.raw.ilikewhatyougot)
        MP_instances.add(R.raw.ilovemorty)
        MP_instances.add(R.raw.imantsinmyeyejohnson)
        MP_instances.add(R.raw.inalieninvasion)
        MP_instances.add(R.raw.inbirdculturethisisconsidered)
        MP_instances.add(R.raw.itsgazorpazorpfield)
        MP_instances.add(R.raw.letmeout)
        MP_instances.add(R.raw.lickmyballs)
        MP_instances.add(R.raw.myman)
        MP_instances.add(R.raw.nobodyexists)
        MP_instances.add(R.raw.ohman)
        MP_instances.add(R.raw.ohmygod)
        MP_instances.add(R.raw.ohyeahh)
        MP_instances.add(R.raw.ooooyeahcaaandoo)
        MP_instances.add(R.raw.owee)
        MP_instances.add(R.raw.rick)
        MP_instances.add(R.raw.rickytickytabbybiatch)
        MP_instances.add(R.raw.riggity)
        MP_instances.add(R.raw.rubberbabybabybunkers)
        MP_instances.add(R.raw.screamingsun)
        MP_instances.add(R.raw.showmewhatyougot)
        MP_instances.add(R.raw.shutthefuckupaboutmoonman)
        MP_instances.add(R.raw.thankyou)
        MP_instances.add(R.raw.thatsretarded)
        MP_instances.add(R.raw.thisishowyoudreambitch)
        MP_instances.add(R.raw.what)
        MP_instances.add(R.raw.whatever)
        MP_instances.add(R.raw.whothefuck)
        MP_instances.add(R.raw.woovuluvubdubdub)
        MP_instances.add(R.raw.yesfuckyougodnottodaybitch)

        val listView = findViewById<ListView>(R.id.samples_list_view)
        listView.adapter = MyCustomAdapter(this,sample_names_const)

        val mediaPlayer = MediaPlayerSingleton.instance
             //   mp = MediaPlayer.create (this, R.raw.aids)



            fun share_audio( file_path: String ){
                val audio =  Uri.parse(file_path)

                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.type = "audio/mpeg"
                shareIntent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                shareIntent.putExtra(Intent.EXTRA_STREAM, audio);

                startActivity(Intent.createChooser(shareIntent, "Rick Morty SB share..."))

            }

fun save_to_Downloads(sample_raw: Int, sample_name: String){

    permissions.setupPermissions(this)
    val raw1 = resources.openRawResource(sample_raw)
    val dataraw1= raw1.readBytes()


    var file1: File
    val outputStream1: FileOutputStream
    try {
        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS  ),"/"+ "RickandMortySB")
        if (!file1.exists()) {
            file1.mkdir();
        }
        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS  +"/"+ "RickandMortySB"), sample_name +".mp3")
        outputStream1 = FileOutputStream(file1)
        outputStream1.write(dataraw1)
        outputStream1.close()
        Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Downloads/RickandMortySB/"  ,Toast.LENGTH_LONG).show()
    } catch (e: IOException) {
        e.printStackTrace()
        Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
    }

}

        fun set_ringtone(sample_raw: Int,sample_name: String){
            permissions.setupPermissions(this)

            // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(applicationContext)) {
                    val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                    startActivityForResult(intent, 200)

                }
                if (Settings.System.canWrite(applicationContext)) {
                    val raw1 = resources.openRawResource(sample_raw)
                    val dataraw1= raw1.readBytes()


                    var file1: File
                    val outputStream1: FileOutputStream
                    try {

                        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_RINGTONES),sample_name +".mp3")
                        outputStream1 = FileOutputStream(file1)
                        outputStream1.write(dataraw1)
                        outputStream1.close()
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Ringtones/",Toast.LENGTH_LONG).show()
                     //SET RINGTONE
                        val values =  ContentValues()
                        values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                        values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                        values.put(MediaStore.MediaColumns.SIZE, file1.length());
                        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                        values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                        values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, true);
                        values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, false);
                        values.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);
                        values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                        var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                        this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                        //Ok now insert it
                        var newUri = this.getContentResolver().insert(uri, values);
                        RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_RINGTONE, newUri);
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as RINGTONE"  ,Toast.LENGTH_LONG).show()

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                    }

                }
            }


        }

        fun set_notification(sample_raw: Int,sample_name: String){
            permissions.setupPermissions(this)

            // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(applicationContext)) {
                    val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                    startActivityForResult(intent, 200)

                }
                if (Settings.System.canWrite(applicationContext)) {
                    val raw1 = resources.openRawResource(sample_raw)
                    val dataraw1= raw1.readBytes()


                    var file1: File
                    val outputStream1: FileOutputStream
                    try {

                        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_NOTIFICATIONS),sample_name +".mp3")
                        outputStream1 = FileOutputStream(file1)
                        outputStream1.write(dataraw1)
                        outputStream1.close()
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Notifications/",Toast.LENGTH_LONG).show()
                        //SET RINGTONE
                        val values =  ContentValues()
                        values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                        values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                        values.put(MediaStore.MediaColumns.SIZE, file1.length());
                        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                        values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                        values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, false);
                        values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, true);
                        values.put(MediaStore.Audio.AudioColumns.IS_ALARM, false);
                        values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                        var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                        this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                        //Ok now insert it
                        var newUri = this.getContentResolver().insert(uri, values);
                        RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_NOTIFICATION, newUri);
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as NOTIFICATION"  ,Toast.LENGTH_LONG).show()

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                    }

                }
            }


        }

        fun set_alarm(sample_raw: Int,sample_name: String){

            permissions.setupPermissions(this)
            // val path = Uri.parse("android.resource://bazarbazaraps.rickapp/raw/"+ sample_name)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!Settings.System.canWrite(applicationContext)) {
                    val intent = Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, Uri.parse("package:$packageName"))
                    startActivityForResult(intent, 200)

                }
                if (Settings.System.canWrite(applicationContext)) {
                    val raw1 = resources.openRawResource(sample_raw)
                    val dataraw1= raw1.readBytes()


                    var file1: File
                    val outputStream1: FileOutputStream
                    try {

                        file1 = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS),sample_name +".mp3")
                        outputStream1 = FileOutputStream(file1)
                        outputStream1.write(dataraw1)
                        outputStream1.close()
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully saved in: Alarms/",Toast.LENGTH_LONG).show()
                        //SET RINGTONE
                        val values =  ContentValues()
                        values.put(MediaStore.MediaColumns.DATA, file1.getAbsolutePath());
                        values.put(MediaStore.MediaColumns.TITLE, file1.getName());
                        values.put(MediaStore.MediaColumns.SIZE, file1.length());
                        values.put(MediaStore.MediaColumns.MIME_TYPE, "audio/mp3");
                        values.put(MediaStore.Audio.AudioColumns.ARTIST, this.getString(R.string.app_name));
                        values.put(MediaStore.Audio.AudioColumns.IS_RINGTONE, false);
                        values.put(MediaStore.Audio.AudioColumns.IS_NOTIFICATION, false);
                        values.put(MediaStore.Audio.AudioColumns.IS_ALARM, true);
                        values.put(MediaStore.Audio.AudioColumns.IS_MUSIC, false);
                        var uri = MediaStore.Audio.Media.getContentUriForPath(file1.getAbsolutePath());
                        this.getContentResolver().delete(uri, MediaStore.MediaColumns.DATA + "=\"" + file1.getAbsolutePath() + "\"", null);

                        //Ok now insert it
                        var newUri = this.getContentResolver().insert(uri, values);
                        RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_ALARM, newUri);
                        Toast.makeText(this,"File "+sample_name +".mp3 succesfully set as ALARM"  ,Toast.LENGTH_LONG).show()

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Toast.makeText(this,"Opsss something goes wrong..."  ,Toast.LENGTH_LONG).show()
                    }

                }
            }


        }

            listView.setOnItemClickListener {
                parent, view, position, id ->


                mediaPlayer.reset_sound(MP_instances[position],this)

                mediaPlayer.MPplay( MP_instances[position],this)




            }

           listView.setOnItemLongClickListener {
               parent, view, position, id ->
               //share_audio("android.resource://bazarbazaraps.rickapp/raw/"+ sample_names_const[position])
                val popupMenu = PopupMenu(this,view)
                popupMenu.setOnMenuItemClickListener {item ->
                    when(item.itemId){
                        R.id.ItemSHARE ->{
                        share_audio("android.resource://bazarbazaraps.rickapp/raw/"+ sample_names_const[position])
                            true

                        }
                        R.id.ItemSAVE ->{
                            save_to_Downloads(MP_instances[position],sample_names_const[position])
                            true
                        }
                        R.id.ItemRINGTONE->{
                            set_ringtone(MP_instances[position],sample_names_const[position])
                            true
                        }
                        R.id.ItemNOTIFICATION->{
                            set_notification(MP_instances[position],sample_names_const[position])
                            true
                        }
                        R.id.ItemALARM->{
                            set_alarm(MP_instances[position],sample_names_const[position])
                            true
                        }
                        else -> false
                    }
                }
               popupMenu.inflate(R.menu.popup_menu)
               popupMenu.show()
              true
            }


        }
    private class MyCustomAdapter(context: Context?, private val sample_names_const: ArrayList<String>) : ArrayAdapter<String>(context, -1, sample_names_const){



        private val  sample_names = arrayListOf<String>(
                    "AIDS",
                    "A long time ago",
                    "And that's the way news goes",
                    "And that's why I always say",
                    "Awww Bitch",
                    "Baby legs",
                    "Burger Time!",
                    "Butthole",
                    "Disqualified",
                    "Get schwifty in here",
                    "Get Schwifty",
                    "Good Job!",
                    "Help!",
                    "Hi I'm mr meeseeks look at me",
                    "Hit the sack jack",
                    "I don't  give a fk you think",
                    "I like what you got",
                    "I love Morty",
                    "I'm ants in my eye Johnson",
                    "In Alien Invasion",
                    "In bird culture this is considered",
                    "It's gazorpazorpfield",
                    "Let me out",
                    "Lick my balls",
                    "My man",
                    "Nobody exists",
                    "Oh man",
                    "Oh my god",
                    "Oh yeahh",
                    "Oooo yeah caaan doo!",
                    "Owee",
                    "Rick",
                    "Ricky ticky tabby biatch",
                    "Riggity",
                    "Rubber baby baby bunkers",
                    "Screaming sun",
                    "Show me what you got!",
                    "Shut the fuck up about moonman",
                    "Thank you",
                    "That's retarded",
                    "This is how you dream bitch",
                    "What",
                    "Whatever",
                    "Who the fuck",
                    "Wubba lubba dub dub",
                    "Yes fuck you god, not today bitch"

            )

            private var mContext = context
            init{
                mContext = context
            }
            override fun getCount(): Int {
                return sample_names.size
            }
            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
               //val textView = TextView(mContext)
                //textView.text="row"
               // return textView



this.notifyDataSetChanged()

                val layoutInflater = LayoutInflater.from(mContext)
                val rowMain= layoutInflater.inflate(R.layout.row_main, parent, false)
                val sampleTextView = rowMain.findViewById<TextView>(R.id.sampletextView)
                sampleTextView.text = sample_names[position]
                var add2fav = rowMain.findViewById<Button>(fav_button) as ToggleButton
                    var fav_table = ArrayList<String>()

                    var dbhandler = MyDBHandler(context ,null, null, 1)
                    val data = dbhandler.getListContents()
                    add2fav.isChecked = false
                    var numRows = data.count
                    if (numRows==0){

                        add2fav.isChecked = false

                    }else {
                        var i = 0
                        add2fav.isChecked = false
                        while (data.moveToNext()) {
//
                            if (data.getString(1) == sample_names_const[position]) {
                                add2fav.isChecked = true
                            }
//
                        }
                    }
             //


                add2fav.setOnCheckedChangeListener{buttonView, isChecked ->
                    if (isChecked) {

                        if (fav_table.size == 0) {
                            //Toast.makeText(mContext, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                            //fav_table.fav_table.add(sample_names_const[position])
                            //fav_table.fav_names_table.add(sample_names[position])
                            dbhandler.addWorker(sample_names_const[position], sample_names[position])
                        } else {
                            var unique = true
                            for (item in fav_table) {
                                if (item == sample_names_const[position]) {
                                    unique = false
                                }

                            }
                            if (unique == true) {
                                //fav_table.fav_table.add(sample_names_const[position])
                                //fav_table.fav_names_table.add(sample_names[position])
                                dbhandler.addWorker(sample_names_const[position], sample_names[position])

                            }
                        }

                    }else{
                        //Toast.makeText(mContext, "hujwwwww" + (position + 1).toString(), Toast.LENGTH_LONG).show()

                        dbhandler.removeWorker(sample_names_const[position])
                    }
                }

                /*
                add2fav.setOnClickListener(object :  View.OnClickListener {


                    override fun onClick(v: View?) {

                        if (checked) {

                            if (fav_table.fav_table.size == 0) {
                                Toast.makeText(mContext, "huj" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                                //fav_table.fav_table.add(sample_names_const[position])
                                //fav_table.fav_names_table.add(sample_names[position])
                                dbhandler.addWorker(sample_names_const[position], sample_names[position])
                            } else {
                                var unique = true
                                for (item in fav_table.fav_table) {
                                    if (item == sample_names_const[position]) {
                                        unique = false
                                    }

                                }
                                if (unique == true) {
                                    //fav_table.fav_table.add(sample_names_const[position])
                                    //fav_table.fav_names_table.add(sample_names[position])
                                    dbhandler.addWorker(sample_names_const[position], sample_names[position])

                                }
                            }
                            checked = false
                        }else{
                            Toast.makeText(mContext, "hujwwwww" + (position + 1).toString(), Toast.LENGTH_LONG).show()
                            checked = true
                        }
                    }
                })*/


                return rowMain
            }

            override fun getItemId(position: Int): Long {
                return position.toLong()
            }

            override fun getItem(position: Int): String? {
                return "test"
            }
           


        }




    }







